
# start: Alone

Monument Valley is unforgiving at the best of times. It's a wide-open stretch
of desert, surrounded by desert, surrounded by mountains. The sun beats down
relentlessly, baking the ground, the riverbeds, and any creatures unfortunate
enough to lack cover. The flora consists of tufts of yellow- green grass
poking through the parched layer of soil; near the riverbeds, there are
sometimes small bushes, no higher than your knee, and occasionally some small
oak trees. The fauna, such as it is, tends to avoid straying beyond cover
during the daylight hours.

---

Slowly, you open your eyes. The hot sun beats down onto your face; you can
feel the sweat pooling on your brow and cascading down your neck. Your mouth
is dry, and your toungue pulls off the roof of your mouth with a bit of
effort. The bright sky temporarily blinds you, and you blink tears away as
your eyes adjust. Above you, the deep blue sky is completely clear, horizon-
to-horizon. The sun is about twenty degrees from the zenith, with no
indication of cardinal directions. Judging by the temperature, it's sometime
in the afternoon.

%% This is correct.

You slowly push yourself up to a sitting position. The ground beneath you is
hard and unforgiving, and you can feel the hard dirt putting indentations into
your elbows. Pulling an arm up, you brush the dirt and debris out of your
hair. Looking around, you see the scattered sandstone buttes that the area is
known for. You don't recognize the various shapes; you've only seen the
landscape in movies and documentaries, but never in person.

You're wearing a T-shirt, short pants, and hiking boots. About a dozen feet
away is a brown tarp, covering up something stacked on the ground. 

> [Investigate the pile](+eq-pile-inv)

# eq-pile-inv

Walking over to the pile with stiff legs, you stumble a few times. You touch
the tarp, but the surface is very hot and you tug your hand back in surprise.

%% The tarp has been here for a long time, due to the heat.

Pulling on a metal grommet, you pull the tarp off the pile. There are two
reinforced yellow plastic boxes underneath, each one about the size of a
carryon bag. There's a handle between the twin black plastic latches that hold
the boxes shut. One of them is marked "Supplies," and the other is marked
"Communications." There are no stickers, stencils, tags, or any other signs of
ownership.

Behind them, there's a hiking backpack. It looks pretty full already.

> [Open the "Supplies" box](+eq-pile-inv-supplies)
> [Open the "Communications" box](+eq-pile-inv-comm)

# eq-pile-inv-supplies

You lift up the supplies box and pull it away from the pile. The box is be
pretty heavy. Snapping back the latches and pulling the lid open, you look at
the inside. appears

# keep-sleeping-1:The First Day

Lying down and pulling the covers over his head, he tried to sleep. _No point
in doing anything with this kind of weather,_ he thought.

[Get up](get-up-1)

# end: The End

endfoobar