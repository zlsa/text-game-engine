
class Book {

  constructor(url) {

    this.chapters = {};

    this.path = [];

    window.onclick = (e) => {
      if(e.target.localName == 'a') {
        this.clicked(e.target);
        return false;
      }
    };

    window.onpopstate = (event) => {
      this.setState(event.state.state);

      return false;
    };

    this.chapter_title_element = document.getElementById('chapter-title');
    this.chapter_contents_element = document.getElementById('chapter-contents');
    this.md = new Remarkable({
      typographer: true
    });

    if(localStorage.getItem('path')) {
      this.path = JSON.parse(localStorage.getItem('path'));
    }

    this.download(url);
  }

  success(event) {
    let text = event.target.responseText;

    let chapter_re = /^\s*#\s+([a-zA-Z0-9\-_]+)(\s*:(.+))?/;

    let lines = text.split(/\n/);

    let chapter_mark = '';
    let chapter_name = '';
    let chapter = '';
    let in_comment = false;

    for(let i=0; i<lines.length; i++) {
      let m = lines[i].match(chapter_re);

      if(m) {

        if(chapter_mark) {
          this.chapters[chapter_mark] = {
            name: chapter_name.trim(),
            contents: chapter.trim()
          };
          chapter = '';
        }

        chapter_mark = m[1];
        chapter_name = m[3];

        if(!chapter_name) chapter_name = '';

        continue;
      }

      if(lines[i].startsWith('<!--')) {

        if(lines[i].endsWith('-->'))
          continue;

        in_comment = true;
        continue;
      }

      if(in_comment) {
        if(lines[i].endsWith('-->'))
          in_comment = false;
        continue;
      }

      if(!chapter_mark) continue;

      chapter += lines[i] + '\n';
    }

    if(chapter_mark) {
      this.chapters[chapter_mark] = {
        name: chapter_name.trim(),
        contents: chapter.trim()
      };
    }

    if(this.path === []) {
      window.history.replaceState({ state: this.getState() }, 'title', '');
      this.go('start');
    } else {
      this.replay(this.path);
    }
  }

  getState() {
    return this.path;
  }

  setState(state) {
    this.replay(state);
  }

  replay(path) {
    console.log('replaying', path);

    this.path = [];

    this.load('start');

    for(let i=0; i<path.length; i++) {
      this.go(path[i]);
    }

  }

  clicked(element) {
    let mark = element.getAttribute('href');

    if(mark.startsWith('+')) {
      element.remove();
    }

    this.go(mark);
  }

  download(url) {
    this.request = new XMLHttpRequest();
    this.request.addEventListener("load", (event) => {
      this.success(event);
    });
    this.request.open("GET", url);
    this.request.send();
  }

  go(mark) {
    if(mark === '<<back>>') {
      if(this.path.length == 1) {
        return;
      }

      this.path.pop();

      this.go(this.path.pop());
      return;
    } else if(mark === '<<restart>>') {
      this.path = [];

      this.go('start');
      return;
    }

    if(!this.load(mark)) {
      return;
    }

    this.path.push(mark);

    localStorage.setItem('path', JSON.stringify(this.path));

    window.history.pushState({ state: this.getState() }, 'title', '');
  }

  load(mark) {
    let append = false;
    let remove_options = true;

    if(this.path.length >= 1) {
      append = true;
    }

    if(mark[0] == '+') {
      mark = mark.substr(1);
      append = true;
    } else if(mark[0] == '=') {
      mark = mark.substr(1);
      append = true;
      remove_options = true;
    }

    let chapter = this.chapters[mark];

    if(chapter == null) {
      console.log('that chapter does not exist');
      return false;
    }

    let rendered_html = this.md.render(chapter.contents);

    if(append) {

      if(chapter.name)
        this.chapter_title_element.textContent = chapter.name;

      let element = this.chapter_contents_element.getElementsByTagName('blockquote');

      if(!element) {
        element = this.chapter_contents_element;
      } else {
        console.log(element);
        element = element[element.length - 1];
      }

      let html_element = document.createElement('div');
      html_element.innerHTML = '<hr class="chapter-separator" />' + rendered_html;

      element.parentNode.insertBefore(html_element, element);

      if(remove_options)
        element.remove();

    } else {
      this.chapter_title_element.textContent = chapter.name;
      this.chapter_contents_element.innerHTML = rendered_html;
    }

    return true;
  }

}

window.book = new Book("proof.md");