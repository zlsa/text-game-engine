
# start: Living in a Comedic Cyberpunk Dystopia

The security officer staggers towards me, the plasma gun waving in his hand,
his polished badge gripped tightly in the other. "Stop reisting!" he gasps.

> [Resist](=resist)
> [Stop resisting](=stop-resisting)

# resist

"Fuck off," I say, and shoot him in the face with my gun. The stream of water
dripping off his nose seems to surprise him, before he looks down at his
now–wet shirt. "Shit," he whines. "These plasma guns ain't waterproof."

"Not my problem, buddy," I say as I sprint past him to the safety of the Wall.

"Don't call me buddy!" he shouts at my retreating back, his plasma gun
sparking uselessly at his side.

I jump over the Wall, and give him the finger with both hands as I fall down
to the ground on the other side.

"Don't call me buddy!" he cries out one last time, muffled by the Wall and the
distance.

I ignore the voice and jog to the safehouse, where the Boss is certain to be
waiting for me.

> [Meet the Boss](=boss)

# boss

"Tell me you got the goods," sighs the Boss. "I can't lose no more jobs."

"Don't worry," I tell him confidently, leaning against the counter. "I got the
goods."

***

"Well? Show it!" he demands after a few moments.

I shuffle uncomfortably. "Well," I stammer, "I don't actually have them here
yet. The Wall is pretty fuckin' tough to smuggle shit past, you know–"

"Oh shut the FUCK UP!" roars the Boss, slamming his fists on the counter. The
whole building shakes. "I pay you to smuggle, you FUCKING SMUGGLE! Ain't
nothing too complicated for your simple head!"

> [Who you calling simple-headed](=simple-headed)
> [Alright, alright, calm down](=calm-down)

# stop-resisting

"Okay!" I shout, placing the water gun on the ground and putting my hands up.
"Don't shoot!"

"Stop resisting!" he shouts, then speaks into his radio: "Backup needed at
Larson and Pine. Suspect resisting arrest. Requesting all units! All units!"

He's still staggering towards me, waving his gun erratically through the air.
He's swaying so much I don't even see it when he pulls the trigger and shoots
me. The world goes the same shade of black that movies use to depict being
shot.

***

When I come to, I'm in a jail cell. It's pretty small, but it's big enough for
my arms and legs to fit inside, which is more than you can say for most jail
cells.

Stretching my arms, I inadvertently break the ceiling of the cell. It's not
really relevant, because I couldn't get past the walls of the jail even if I
was out of my cell. Stupid anti-robot legislation.

I poke my head through the roof and spot the officer who locked me in.

"Hey, buddy," I call out, and he jogs over. "Hey," he says, "Ain't you the one
that shot me?"

"Shot you? No clue what you're talking about, buddy," I say. "Anyways, I've
got like sixteen billion creds. I'll give you half if you reprogram me to
clear the walls."

"Done," he says instantly, and before I can get another word out, he's jacked
me in and cleared me of all crimes.

"Thanks, buddy," I say as I climb through the hole in the roof. I turn around
and spray him with my watergun. He pales and steps back as I unfold my 9-foot-
tall titanium frame. I stride over to the fence and walk straight through
without any side effects.

I jog to the Wall and jump over it, heading for the safehouse, where the Boss
is sure to be.

> [Meet the Boss](=boss)

# simple-headed

"Who you calling simple-headed, Boss? 'Cause I'm finding it more than a tad
ironic."

He rears back in anger. "OH NO YOU DON'T," he roars, smashing his fists
against his chest like an alpha male. "YOU DON'T GET TO CALL ME SIMPLE-
HEADED–"

His chest sparks where his fists are impacting, and he droops down to the
counter. A downside of the aggressive models is that they're quite capable of
self-termination, and if given the right stimuli, can perform it single-
handedly in just a few seconds. Well, he used both hands, but the point still
stands.

Mission accomplished.

I pull out my Corporation-issued phase-array handcuffs and snap them on around
his limp wrists. The Corporation will get the signal that the handcuffs were
deployed and send a retrieval team to pick up the body.

The Corporation will be very happy with me today. I made a crime mobster
self-destruct _and_ annoyed a disposable security officer. I might even get an
arm upgrade.

## The end.

# calm-down

"Whoa there, Boss. Calm down. I ain't bullshitting you this time, I swear."

The Boss rears back in anger. "That's what you said the last four fuckin'
times!"

"Well, yes," I admit, "but that's not true this time."

"Then where the goods at?" demands the Boss.

"The poor security officer's got it," I say. "I sprayed it all over him. The
Company's never gonna find it."

His eyes go red. That's never a good sign. He leans forward in what I suppose
would be an intimidating manner, if I hadn't had my fear module removed.

"You fuckin' sprayed the most valuable data I ever got onto a fuckin' security
flunkie?" roars the boss. I'm actually pretty impressed; they even got the
spittle. Pretty advanced for a body of his... level.

"Yes, sir," I say. "It's easy. We wait for the uniform truck, then hijack it
and nab his suit." I really hoped the Boss went for this plan, because if he
didn't, the uniform company would wash the officer's clothes, and we'd lose
the data.

The Boss sat back and thought for a minute. It wasn't real thought, of course.
His programming was entirely deterministic, and these delays were added only
for effect. After 34 seconds of "thinking", he leaned forward, eyeing me.

"We goin' with plan A. You're leading. Don't fucking fail me this time!"

> [Plan A](plan-a)
> [Plan B](=plan-b)

# plan-a: Plan A

The Boss is always right, and I make sure to tell him. It keeps him happy.

"Yes, boss," I dutifully respond. "I'll be there."

***

I'm there. I watch as the uniform truck makes its way around the corner.

I step out of the shadows and point my rail-machine-gun at the driver and
engage my megaphone. "Step out of the vehicle," I shout. "I am authorized to
use lethal force!"

He slams on the brakes, and the tires squeal as the van slides to a stop. He
jumps out and runs away, shouting "I don't get paid enough for this shit!"

I jump to the van and rip the roof off, then find the stained police uniform
and tug it out. The nanodatacubes in the water I'd sprayed on him earlier are
extracted with a flick of a cotton swab, and I analyze the data contained in
them.

The Boss is smuggling... captchas and their solutions? Really? The
nanodatacube contained about 3.8 billion captchas and the corresponding
solution. Then I remembered seeing the ads on the seedier websites: "Pay us
cold, hard cryptocurrency and we'll solve your captchas with AI and machine
learning and neural networks!" Oh. Well, at least I knew what the Boss was up
to. Apparently, there was a lot of money in automatically solving captchas.

Well, time to get it to him.

> [Bring the data to the Boss](=data-to-boss)

# data-to-boss

"Here you go, Boss," I say. "All the data, just like you asked."

"Good job," he says, leaning over the counter to retrieve the nanodatacubes.
"If I could get you more cash, I would. I'm low on the dough, though."

"Understood," I say, walking away. "Aren't you glad you paid me the big bucks
to captcha the data?"

"What?" he said, befuddled, as I walked out through the door.

The Corporation won't be happy. I did my job perfectly, and now the crime
mobster can stay in business for another day. But I have to say, the money
with the Boss is way better than anything the Corporation could offer me. My
conscience, if I had one, would be screaming out to not help the Boss. But in
this world, money is money and time is time, and I'm short on both. Looks like
I'll have the money for a recharge tonight, though. At least there's that.

## The end.

# plan-b

"Plan B it is, Boss," I say blithely as he turns away.

"What the FUCK did you just say about me, you LITTLE BITCH!" he roars. "I'LL
HAVE YOU KNOW I FUCKED UR MUM!"

"Yeah yeah, whatever," I say, pulling out my rail-machine-gun. I charge it up
and empty the full clip directly into the Boss. He staggers back, puts his
hands to his chest and through the giant hole in it, and topples backwards to
the floor.

I pull the rail-machine-gun up and blow the smoking barrels. "Perfect shot," I
say to myself.

I pull out my Corporation-issued phase-array handcuffs and snap them on around
his limp wrists. The Corporation will get the signal that the handcuffs were
deployed and send a retrieval team to pick up the body.

Putting the rail-machine-gun away, I leave the building as inconspicously as a
9-foot-tall robot with an 8-foot-long gun can. The gun is literally smoking,
but still, nobody sees me leave.

Mission accomplished.

The Corporation will be extra happy with me today. Thwarted a crime mobster
_and_ annoyed a disposable security officer. I might even get an arm upgrade.

## The end.
