
# start: The Last of Us: Alone

"Oh shit," Ellie cries when she sees the section of metal protruding from Joel.
"Oh man. What do you want me to do?"

"Move," pants Joel, and when she doesn't respond, he reaches out to pull her
back, clearing the line of fire. "Move!" he shouts, as the door bursts open
and two hunters emerge, guns drawn. He pushes himself off the ground with his
left arm and shoots them both, then relaxes back onto the ground as they fall.
He can feel himself losing strength.

"Joel?" asks Ellie.

"I need you to pull," grunts Joel. Ellie grabs his hand and braces her legs
beneath her. "One–two–three!" She tugs him up, and he almost loses
consciousness. She catches sight of the blood oozing out of the hole in his
stomach as he falls to his knees.

"Just get to the damn horse," Joel whispers, and Ellie helps him get to his
feet. He takes a step, shakily, and then another as Ellie clears the room the
hunters came from.

"Joel? How we doing?" she asks.

"I'm fine, I'm fine," Joel grunts, but she can tell how much pain he's in.

"Can you handle the window?" She's found a broken window, and she pushes some
planks aside to clear the way, then vaults through.

"Yeah," Joel whispers. But he isn't strong enough, and he collapses on the
other side. "Come on! Move!" shouts Ellie, pulling him up.

She looks up at the sound of a door being kicked open. "I see you!" shouts a
bandit, waving around a shotgun.

"Alright, just stay here," Ellie whispers to Joel, propping him up against a
counter. "I'm gonna flank this asshole."

"Ellie," groans Joel as she darts around the room.

"Those were my friends you killed, asshole!" shouts the bandit, shooting at
Joel. He gets off another shot at Joel, and before he can reload, Ellie
manages to distract him. "Oh, you little bitch," growls the man. Joel leans
out and squeezes off two shots, and the man falls to the floor.

Ellie runs back to Joel, helping him up. "Oh man. We gotta get you out of here."

"I'm okay," Joel grunts.

"You're not okay, Joel!" Ellie shouts. "Come _on!_ Move it! There's nobody here."

She leads the way out of the university corridors. "There's nobody here.
You're doing good."

He groans in response, staggering after her and holding an arm to his stomach.
As they approach the hallway, he slides down against a filing cabinet. Ellie
runs to his side and kneels down. "Here, lean on me." Joel shakes his head.
"No," he says, pushing himself upright.

"Well, can you walk?" asks Ellie. "Yes," groans Joel. "Well then, fucking
walk!"

They make their way along the hallway to the exit. "There's the exit," Ellie
says. "Just a little bit more, come on."

Joel staggers again, and Ellie turns around to help. "Behind you," Joel
groans. "What?" asks Ellie.

"Ellie!" shouts Joel, managing to shoot at them before he falls unconscious.

"The kid's got a gun!" shouts one of them, but before they can even move,
Ellie shoots them both. "Joel!" she shouts, turning around, but one of the
hunters runs at her. "You little shit!" he shouts as he bears down on her with
a crowbar, but before he has a chance to swing it at her face, she shoots him
in the stomach and he falls back, arms limp, crowbar clattering to the
linoleum.

Ellie dashes back to Joel's side. "Put your arm around me," she instructs,
helping him to his feet. "All right–let's go."

They begin the walk to Callus.

"I swear to god," Ellie pants, "I get you out of this, you are _so_ singing
for me."

"You wish," moans Joel, coughing.

At the door, Ellie stops, letting him lean against a fallen table. "I'll get
the door."

"Take your time," Joel whispers faintly, holding his wound.

The door pushes open and she turns back to him. "Come on."

He stumbles out the door and half-pushes, half-falls onto the outer door. He
rolls down the steps, and hears Callus, and hears the reins shaking.

"What the fuck–" says a voice, but before it can finish, Ellie shoots the
hunter, and runs down the stairs to help Joel up.

"Come on," she says, almost begging him, but Joel stops here. "Just... just
get the horse," he grunts.

"Okay," she says, dashing to Callus and leading him over as Joel gets to his feet.

"Can you get on?" she asks him, and he lets go of his wound, groaning in pain,
and pulls himself up onto the saddle.

He grabs the reins and shakes them. "Go," he whispers at Callus, and the horse
takes off.

> [Next](ellie-1)

# ellie-1: The Last of Us: Alone

I was never taught much about fixing people up. Oh sure, the military taught
me how to wrap wounds so they'd stop bleeding and shit, but it was just so you
could help the injured get to a hospital, where they'd be fixed up properly,
by a trained doctor.

Now, I need to fix up Joel. He was hurt real bad with that rebar; I've gotten
the bleeding stopped and disinfected the wound (thank fuck he was
unconscious), but there's a real good chance it's infected. If it is, I don't
know what the hell I can do. It's not like we carried penicillin or shit like
that.

***

I'm not sure exactly where we are. I mean, I know what town we're in, but that
doesn't really matter anymore. The maps don't tell me where the people are
now, where the little communities like Jackson are. I can't move Joel, not
with this weather, and I can't leave him in the resort overnight without a
fire burning, which has all of the obvious downsides.

So I keep on going. I've learned to use Joel's bow, and I've been killing
small animals like rabbits, bringing them back to the resort, preparing them,
and helping Joel eat the broth during the rare times he's half-awake.

***

Joel hasn't woken up yet; not properly, anyway. It's been two months now, and
he wasn't doing any better or worse until today. It was bound to happen; you
might get lucky and stay away from the clickers, but you can't escape
infection.

He's in a coma now. I don't know what to do.

***

I'd just shot and killed a rabbit when I heard it. I looked up, and there it
was: a deer. Just standing there, staring at me. Easy shot.

But then some asshole who can't even shoot a fuckin' stationary target fires
at the buck, and misses. Its ears perk up, it shakes its head, and it runs
away as quickly and quietly as it came.

_Shit._

I quietly run after it. It can't have run _that_ far. Most animals nowadays
don't seem too skittish 'round humans. I follow its prints to the edge of a
clearing; in the middle, no more than fifty feet away, stands the buck,
antlers standing proud against the blue sky.

Swinging my bow upright and pulling the string taut, I aim and fire. My arrow
hits the deer in its neck, and it jumps up and cries out, almost like a human.
_We need the meat,_ I justify. _Rabbits just aren't enough._

I run after it, all pretense of stealth forgotten. I eventually come up to

"Fuck," I whisper to myself, holding up the bow as I scan the trees around me.
I feel a presence behind me and swing around.

"Whoa! Whoa! Don't shoot!" says the man on the left, holding up his hands. "We
don't want anyone to get hurt." Leaning over to his partner, he instructs him
to drop his weapon.

"Any sudden moves and I put one between your eyes. Same goes for buddy-boy
over there," I add, swinging the bow to his partner in what I hope is a
threatening manner.

"We don't want any trouble," says the man on the left. "My name's David, and
this is James. We're from a larger group, with women and children, and we're all
very, very hungry," he says, his eyes never leaving me. He gives me the
fucking creeps. "Could we perhaps trade our supplies for some of that meat?"

On impulse, I blurt, "Do you have medicine? Antibiotics?" Shit. I shouldn't've
said that.

"We do, back at the camp," says David. "You're welcome to follow us–"

"I'm not following you anywhere," I retort. "Buddy-boy can get it. He comes
back with the medicine, the deer is all yours. Anyone else shows up with him–"

"You put one right between my eyes," says David, almost smiling.

That's right," I tell him.

David turns to James. "Two bottles of penicillin and a syringe," he says.
"Make it fast." James nods, leaving the rifle on the ground. I can see that
David is trying to not look at it.

"I'll take that rifle," I say.

"Of course," says David, his arms wide.

"Back up," I demand, and he nods and backs up a few steps. I dart forward and
retrieve the rifle, then step back and swap the bow for the rifle.

"He's, ah, he's probably gonna be a while," says David. "You mind if we take
some shelter from the cold?"

"Bring him with us," I say, gesturing at the deer. He complies, and we make
our way to an old barn. David breaks down the door, and he drags the deer
inside and begins to look for materials to start a fire.

***

"You really shouldn't be out here on your own," says David as we stand around
the fire.

"I don't like company," I reply.

"I see," says David, looking at me sidelong. "What's your name?"

"Why?" I ask.

"Look," David says, turning to face me. "I understand it's not easy to trust a
couple of strangers. Whoever's hurt, you clearly care about him. I'm sure it's
gonna be just fine."

"We'll see," I say. I hear a faint noise outside, and there, right on the
other side of the room, is a clicker.

"Shit!" I say, and David sees it and kills it before I can pull out the rifle.

"You had another gun?" I should've checked him more thoroughly.

"Sorry," he says sheepishly. "Can I have my rifle back now?"

<!-- > [Give him the rifle](=ellie-1-none) -->
> [Keep the rifle](=ellie-1-rifle)
> [Keep the rifle and take the pistol](=ellie-1-rifle-pistol)

<!--
# ellie-1-none

I think about it for a second. He hasn't done anything to me yet, and there
_are_ going to be more clickers soon.

"You a good shot?" I ask.

"Would I be hunting if I wasn't?" he replies.

"Keep that thing pointed at the clickers and I won't kill you," I tell him,
handing him his rifle.

-->

# ellie-1-rifle

"No," I inform him. "You still have your pistol."

"I hope you know how to use that thing," he says.

"I've had some practice."

"We'd better lock up the doors and windows," he says, closing the door and
pulling a shelf down in front of it. "Probably more where that one came from."

***

He's right. Only a few seconds after pulling a tarp over the deer, a clicker
bursts through a window, clicking wildly. I swing the rifle around and pull
off a headshot. The sound of the rifle brings more clickers, and soon they're
all around us. David and I are back-to-back now, and we're killing them as
fast as they're coming in.

# ellie-1-rifle-pistol

"No," I inform him. "And give me your pistol. You never said you had one."

"You never asked," he counters. "There's bound to be more where that one came
from. Isn't it best if we're both armed?"

"Not for me it fucking isn't," I retort. "Remember what I said before?"

"Right between the eyes," he says. And he hands the pistol over, albeit
grudgingly.

"Just don't get us killed," he tells me, moving over to the door and pulling a
shelf down in front of it. "You better be as good a shot as you seem to think
you are."

***